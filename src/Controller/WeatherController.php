<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class WeatherController extends AbstractController
{


    public function index(): Response
    {
        $number = random_int(0, 100);

       // return new Response(
         //   '<html><body>Lucky number: '.$number.'</body></html>'
        //);
        return $this->render('weather/weather.html.twig', ['number' => $number,]);

    }

    
}